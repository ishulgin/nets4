#!/usr/bin/env bash

ip a add 4.7.1.3/29 dev eth1
ip a add 4.7.2.3/29 dev eth2
ip a add 4.7.4.3/29 dev eth3

sysctl -w net.ipv4.conf.all.rp_filter=0
sysctl -w net.ipv4.conf.eth1.rp_filter=0
sysctl -w net.ipv4.conf.eth2.rp_filter=0
sysctl -w net.ipv4.conf.eth3.rp_filter=0

ip link set eth1 up
ip link set eth2 up
ip link set eth3 up

sysctl -w net.ipv4.ip_forward=1

ip rule add fwmark 1 table 1
ip rule add fwmark 2 table 2
ip route add default via 4.7.1.1 table 1
ip route add default via 4.7.2.2 table 2
ip route flush cache
iptables -t mangle -A PREROUTING -d 4.7.0.2 -j MARK --set-mark 1
iptables -t mangle -A PREROUTING -d 4.7.0.1 -j MARK --set-mark 2
