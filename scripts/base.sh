#!/usr/bin/env bash

# 1. Запретить передачу только тех пакетов, которые отправлены на TCP-порт, заданный в настройках утилиты nc.
# 2. Запретить приём только тех пакетов, которые отправлены с UDP-порта утилиты nc.
# 3. Запретить передачу только тех пакетов, которые отправлены с IP-адреса компьютера А.
# 4. Запретить приём только тех пакетов, которые отправлены на IP-адрес компьютера Б.
# 5. Запретить приём и передачу ICMP-пакетов, размер которых превышает 1000 байт, а поле TTL при этом меньше 10.

# А (клиент) - nets4
# Б (сервер) - nets1

# nets4
iptables -F
# 1
iptables -A OUTPUT -o eth0 -p tcp --dport 11111 -j REJECT
# 2
iptables -A INPUT -i eth0 -p udp --sport 11111 -j REJECT
# 5
iptables -A OUTPUT -o eth0 -p icmp -m length ! --length 0:1000 -m \
 ttl --ttl-lt 10 -j REJECT


# nets 1
# 3
iptables -A OUTPUT -s 4.7.4.4 -j REJECT
