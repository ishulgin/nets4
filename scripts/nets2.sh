#!/usr/bin/env bash

ip a add 4.7.0.2/29 dev eth1
ip a add 4.7.2.2/29 dev eth2

ip ro add 4.7.4.0/29 via 4.7.2.3

sysctl -w net.ipv4.conf.all.rp_filter=0
sysctl -w net.ipv4.conf.eth1.rp_filter=0
sysctl -w net.ipv4.conf.eth2.rp_filter=0

ip link set eth1 up
ip link set eth2 up

sysctl -w net.ipv4.ip_forward=1

ip rule add fwmark 1 table 1
ip route add default via 4.7.0.1 table 1
ip route flush cache
iptables -t mangle -A OUTPUT -s 4.7.0.2 -j MARK --set-mark 1