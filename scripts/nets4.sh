#!/usr/bin/env bash

ip a add 4.7.4.4/29 dev eth1

ip ro add 4.7.1.0/29 via 4.7.4.3
ip ro add 4.7.2.0/29 via 4.7.4.3

ip link set eth1 up
